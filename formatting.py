from bs4 import BeautifulSoup

class Text:

    def to_key(self, string):

        # Change text string into a key string for a dictionary, making all lowercase and replacing
        # spaces with underscore
        return string.lower().replace(' ', '_')

class Scrape(object):

    soup = object

    def soupify(self, html):

        self.soup = BeautifulSoup(html, 'html.parser')

    def get_elements(self,attr,val,tag=None, occurrence=None):

        if isinstance(self.soup,object):
            if tag is None:
                self.soup = self.soup.find(class_=val)
            elif occurrence is None:
                self.soup = self.soup.find_all(tag, {attr: val})
            else:
                self.soup = self.soup.find_all(tag, {attr: val})[occurrence]
            return self.soup
        else:
            return 'Must first soupify the page'

class List(object):

    master = []

    def merge_lists(self,original=None,append=[]):
        if len(self.master) == 0 and original is not None:
            self.master = original
        for item in append:
            self.master.append(item)
        return self.master
