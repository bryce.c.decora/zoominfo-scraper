# Import all required modules here
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from formatting import Text, Scrape, List
import os



class Browse(object):

    path = os.path.abspath(os.getcwd()) + "/chromedriver"
    # Setup the options for the chrome browser we will be using to navigate the page
    # - incognito option allows us to use an incognito window when scraping
    # --headless is used to stop browser from showing while running code
    option = webdriver.ChromeOptions()
    option.add_argument(" — incognito")
    option.add_argument("--disable-notifications")
    option.add_argument('--disable-blink-features=AutomationControlled')
    option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")

    #option.add_argument("--headless")

    # Must download the 'chromedriver' executable and reference the path here
    # Initiating our browser with our options and chromedriver.exe location
    browser = webdriver.Chrome(executable_path=path, chrome_options=option)
    browser.execute_cdp_cmd('Network.setUserAgentOverride', {"userAgent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.53 Safari/537.36'})
    text = Text()
    scrape = Scrape()
    lister = List()

    obj = {}

    def open_page(self, url):

        # Use the browser initiated in the Browser __init__ to navigate to the URL
        self.browser.get(url)

    def wait_for_load(self, xpath):

        # Wait for the element given the element classes to load on the screen (max 10 seconds)
        wait = WebDriverWait(self.browser, 40)
        wait.until(ec.visibility_of_element_located((By.XPATH, xpath)))


    def input_text(self, text="", xpath=""):

        text_entry = self.browser.find_element_by_xpath(xpath)
        text_entry.send_keys(text)

    def click_element(self, xpath, occurrence=0):

        # Click an element found by its xpath (default find first occurrence)
        self.browser.find_elements_by_xpath(xpath)[occurrence].click()

    def list_contacts(self,occurrence=0):
        contacts = []
        # List all elements with the div tag and the given classes, returning elements as a list
        self.scrape.soupify(self.browser.page_source)
        names = self.scrape.soup.find_all(class_="primary-name")
        contacts = []
        for name in names:
            info = {
                "first_name":       "",
                "last_name":        "",
                "company":          "",
                "employees":        "",
                "phone_1_number":   "",
                "phone_1_type":     "",
                "phone_2_number":   "",
                "phone_2_type":     "",
                "phone_3_number":   "",
                "phone_3_type":     ""
            }
            info['first_name'] = name.text.split(" ")[0].strip("\n")
            info['last_name'] = name.text.split(" ")[-1].strip("\n")
            company = name.find_next("zi-dotten-text", attrs={"class":"company-name-link"}).text.strip("\n")
            info["company"] = company
            employees = name.find_next("zi-row-text", attrs={"class": "employeeCount"}).text.strip("\n")
            info["employees"] = ''.join(filter(str.isdigit, employees))
            data = name.find_next("div", attrs={"class": "profile-container"})
            if data is None: continue
            numbers = data.find_all("a", {"class": "record-data"})
            phone_count = 1
            for number in numbers:
                info['phone_%i_number' % phone_count] = ''.join(filter(str.isdigit, number.text))
                info['phone_%i_type' % phone_count] = "mobile" if "Mobile" in number.text else "other"
                phone_count += 1
                if phone_count >= 3:
                    break
            contacts.append(info)
        return contacts


    def next_page(self):
        try:
            self.browser.find_elements_by_xpath('//*[@id="app-root"]/zi-pages/div/div/div/zi-search-core-container/zi-page-template/div/div/zi-page-content/div/div/div[2]/zi-people-landing/div/zi-people-results-page/zi-people-results-block/div/zi-pagination/div/ul/li[11]/a')[0].click()
        except:
            self.browser.find_elements_by_xpath('/html/body/app-root/zi-pages/div/div/div/zi-search-core-container/zi-page-template/div/div/zi-page-content/div/div/div[2]/zi-people-landing/div/zi-people-results-page/zi-people-results-block/div/zi-pagination/div/ul/li[10]/a')[0].click()
    def submit_text_field(self, input="", field_id="", button_number=2):

        # Submit the form found on the page containing the text 'Search'
        text_entry = self.browser.find_element_by_id(field_id)
        text_entry.send_keys(input)
        self.browser.find_elements_by_xpath("//a[contains(text(), 'Search')]")[button_number].click()

    def get_messenger_key(self):

        # Grab ID from messenger area so we know what to click later
        elements = Browse().list_elements('div', attr="data-testid", value="chat_sidebar")


