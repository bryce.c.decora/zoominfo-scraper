import csv
from scraper import ZoomInfo

zoominfo = ZoomInfo()
first = True
# Gather all contact info and phone numbers
numbers = zoominfo.get_available_phone_numbers()
# Write to file
keys = numbers[0].keys()
with open('number_data.csv', 'r') as csvfile:
    csv_dict = [row for row in csv.DictReader(csvfile)]
    if len(csv_dict) > 0:
        first = False
with open('number_data.csv', 'a', newline='') as output_file:
    writer = csv.DictWriter(output_file, keys)
    if first:
        writer.writeheader()
    writer.writerows(numbers)
