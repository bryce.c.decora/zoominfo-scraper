from WebNav import Browse
from formatting import List
import time
import random


class ZoomInfo():

    browse = Browse()
    lister = List()

    def open_contact_panels(self):
        path = '//*[@id="app-root"]/zi-pages/div/div/div/zi-search-core-container/zi-page-template/div/div/zi-page-content/div/div/div[2]/zi-people-landing/div/zi-people-results-page/zi-people-results-block/div/div[2]/div/zi-person-row[%i]/zi-search-result-row/div/zi-person-classic-view' % 1
        self.browse.wait_for_load(xpath=path)
        for x in range(1, 26):
            try:
                time.sleep(random.randint(1, 4))
                path = '//*[@id="app-root"]/zi-pages/div/div/div/zi-search-core-container/zi-page-template/div/div/zi-page-content/div/div/div[2]/zi-people-landing/div/zi-people-results-page/zi-people-results-block/div/div[2]/div/zi-person-row[%i]/zi-search-result-row/div/zi-person-classic-view' % x
                self.browse.click_element(xpath=path)
            except:
                print("all opened...")

    def get_available_phone_numbers(self):
        numbers = self.browse.list_contacts()
        return numbers

    def next_page(self):
        self.browse.next_page()
