# ZoomInfo Phone Number Scraping

ZoomInfo houses phone numbers we would like to pull with a semi-automated process.
This code will help scrape data from the current page, once all contact panels
are opened.


## Features

  Code relies on the following tools:
  
* **[Selenium](https://pypi.org/project/selenium/)** Controling browser from Python
* **[BeautifulSoup](https://pypi.org/project/beautifulsoup4/)** Pulling web data


## Local Development Setup

#### Install PyCharm

*PyCharm is what you will use to run the Python Code*

- Install PyCharm Community [here](https://www.jetbrains.com/pycharm/download/#section=mac)

### Check whether you have Python 3 Installed

- Open a new terminal window Launchpad > Terminal
- Type the following (without the '$') one line at a time

```sh
$ python3 --version
```

- Skip the next 2 steps if your system shows that you have Python 3

#### Install Homebrew (ONLY NECESSARY IF YOU DON'T HAVE PYTHON 3)

*Homebrew is used to easily install packages.  We will use this to install Python*

- Open new terminal window (Launchpad > Terminal):

```sh
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

#### Install Python 3 (ONLY NECESSARY IF YOU DON'T HAVE PYTHON 3)

*Python 3 is the language used to execude the code*

- Open new terminal window (Launchpad > Terminal):

    > Installs Python 3 using [Homebrew](https://brew.sh/) and checks to ensure the download
     was successful.  You may use your preferred tool to download.

```sh
$ brew install python3
$ python3 --version
```


#### Set Up PyCharm

*Set up PyCharm with a new project that will use Python 3

- Open PyCharm Application and start a New Project (File New Project)
- Choose file location and name
- Click "Project Interpreter"
- Select "New Environment Using Virtualenv"
- Choose Python 3

[![PyCharm Setup][PyCharm Setup]][PyCharm Setup]

#### Download Code

*Scraper code is housed online on GitLab.  Do the following to download it.

- Open a terminal window within PyCharm by clicking terminal on the bottom of the page

[![PyCharm Setup][PyCharm Terminal]][PyCharm Terminal]

- Run the following code in the terminal in PyCharm that you just opened to copy the base code to your project

```sh
$ git clone git@gitlab.com:bryce.c.decora/zoominfo-scraper.git
```

- Run the following code within the terminal to download dependencies

```sh
$ pip3 install -r zoominfo-scraper/requirements.txt
```

#### Start Chrome With Command Line

*Chromedriver is used to open up a browser that we can control with Python

- Open a new terminal window
- Run the following:

```sh
$ export PATH="/Applications/Google Chrome.app/Contents/MacOS:$PATH"
```

- Next run this command to open a controllable chrome window:

```sh
$ Google\ Chrome --remote-debugging-port=9222 --user-data-dir="~/ChromeProfile"
```

#### Open and Log In To Zoom Info

- Get credentials from Kyle on logging in to ZoomInfo on that Chrome page that was just opened
- Navigate to target data (Kyle can help you get to the page required)
- Expand all contacts to reveal phone numbers

#### Strip Phone Numbers from Page

- Right click main.py within PyCharm
- Select > Debug 'main'
- If no errors display after run, you should see all contact information loaded for the page into *number_data.csv*
- Click to the next page
- Select Debug button (green button below) and repeat for all pages

[![PyCharm Setup][Debug Button]][Debug Button]

## Authors

This code is managed by:

| Name | Email |
| ------ | ------ |
| Bryce DeCora | [bryce.c.decora@gmail.com](mailto://bryce.c.decora@gmail.com) |

> Want to join in the fun?  Shoot one of us an email!

[PyCharm Setup]: <https://gitlab.com/bryce.c.decora/zoominfo-scraper/-/raw/main/images/pycharm_setup.png>
[PyCharm Terminal]: <https://gitlab.com/bryce.c.decora/zoominfo-scraper/-/raw/main/images/pycharm_terminal.png>
[Debug Button]: <https://gitlab.com/bryce.c.decora/zoominfo-scraper/-/raw/main/images/debug_button.png>